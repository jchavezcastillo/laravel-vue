<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Model\Users;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('search', ['as' => 'search.users', 'uses' => function(){
    $results = DB::table('users')->where('status', 1)->get();
    return  response()->json(array(
        "results" =>  $results
    )); 
}]);

Route::post('insert', ['as' => 'new.users', 'uses' => function(){
    $data = Input::all();
    $user = new Users();
    $user->name = $data['name'];
    $user->email = $data['email'];
    $user->login = $data['login'];
    $user->remember_token = '2r2r239r324hr349';
    $user->status = 1;
    $user->user_group_id = 1;
    $user->save();
    return  response()->json(array(
        "success" =>  true
    )); 
}]);

Route::post('delete', ['as' => 'delete.users', 'uses' => function(){
    $data = Input::all();
    $user = Users::where('status', 1)
    ->where('name', $data['name'])
    ->first();
    $user->status = 0;
    $user->save();
    return  response()->json(array(
        "success" =>  true
    )); 
}]);