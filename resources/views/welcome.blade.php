<!doctype html>
<!--Tutorial: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties-->
<!--Ciclo de vida: https://alligator.io/vuejs/component-lifecycle/-->
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>

        <title>Laravel</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
        
       
    </head>
    <body>
        <div id="app" class="container">
            <example></example>
        </div>
    <script>
        axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        var urlUser = "{{ route('search.users') }}";
        var newUser = "{{ route('new.users') }}";
        var deleteUser = "{{ route('delete.users') }}";
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
        
    </body>
</html>