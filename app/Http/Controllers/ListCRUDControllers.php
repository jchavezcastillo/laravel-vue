<?php

namespace App\Http\Controllers\Contador\Home;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class HomeCRUDController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        //$this->middleware('role:partner');
    }

    /**
    * Buscador de contribuyentes por nombre fiscal y comercial (física y moral)
    * Url: contadorescorporativos/public/contabilidad/search/contribuyente
    * As: search.contribuyente
    * Petición: GET
    *@return View.
    */
    public function SearchName($search)
    {
        $results = $user = DB::table('users')->where('estatus', 1)->first();
        return  response()->json(array(
            "results" =>  $results
        )); 
    }

}